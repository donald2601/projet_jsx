
import './App.css';
import FirstHeader from './header/FirstHeader';
import Navbar from './navbar/Navbar';
import BodyMain from './body/BodyMain';
import BodyLast from './body/BodyLast';


function App() {
  return (
    
    <div className="App">
      <FirstHeader/>
      <Navbar/>
      <BodyMain/>
      <BodyLast/>
    </div>
  );
}

export default App;
