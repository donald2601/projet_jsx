import './BodyLast.css';


export default function BodyLast() {
  return (
    <div className='body'>
      <div>
        <h3>Déclaratif</h3>

        <p>
        React facilite la création d'interfaces utilisateur interactives.
        Concevez des vues simples pour chaque état de votre application,
        et React mettra à jour et affichera efficacement les bons composants lorsque vos données changent.
        </p>

        <p>Les vues déclaratives rendent votre code plus prévisible et plus facile à déboguer.</p>
      </div>
      <div>
      <h3>Basé sur les composants</h3>

      <p>
      Créez des composants encapsulés qui gèrent leur propre état, 
      puis composez-les pour créer des interfaces utilisateur complexes.
      </p>

      <p>
      Étant donné que la logique des composants est écrite en 
      JavaScript au lieu de modèles, vous pouvez facilement transmettre des données riches via votre application et garder l'état hors du DOM.
      </p>
      </div>
      <div>
      <h3>Apprenez une fois, écrivez n'importe où</h3>

      <p>
      Nous ne faisons aucune hypothèse sur le reste de votre pile 
      technologique, vous pouvez donc développer de nouvelles 
      fonctionnalités dans React sans réécrire le code existant.
      </p>

      <p>
      React peut également effectuer un rendu sur le serveur à l'aide 
      de Node et alimenter des applications mobiles à l'aide de React Native .
      </p>
      </div>
    </div>
  )
}
