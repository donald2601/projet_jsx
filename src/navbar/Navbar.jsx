import logo from '../logo.svg';
import './Navbar.css';
import PropTypes from "prop-types";

const Navbar = () => {
    return (
        <div  className="container">
                <div id='first'> 
                
                    <span> <img src={logo} alt="erreur" /></span>
                    <span id='react'>React</span>
                    
                </div>
                <div id='second'>
                    <ul>
                        <li> <a href="#">Document</a></li>
                        <li> <a href="#">Didacticiel</a></li>
                        <li> <a href="#">Blog</a></li>
                        <li> <a href="#">Communauté</a></li>
                    </ul>
                </div>
                <div id='third'>
                    <ul>
                        
                        <li><input type="text" /></li>
                        <li> <a href="#">V17.02</a></li>
                        <li> <a href="#">Langage</a></li>
                        <li> <a href="#">GitHub</a></li>
                    </ul>
                </div>
        </div>
    );
}

export default Navbar;
