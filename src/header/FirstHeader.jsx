import  './FirstHeader.css';

const FirstHeader = () => {
    return (
        <div>
            <div className="headerOne">
            <p> <span>Soutenez l'Ukraine 🇺🇦 {""}</span>
                <span> Aidez à fournir une aide humanitaire à l'Ukraine.</span>
            </p>
            </div>
        </div>
    );
}

export default FirstHeader;
